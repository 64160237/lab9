package com.jtrps.week10;

public class Triangle extends Shape{
    private double sideA;
    private double sideB;
    private double sideC;

    public Triangle(double sideA, double sideB, double sideC){
        super("Triangle");
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    public double getSideA() {
        return sideA;
    }

    public double getSideB() {
        return sideB;
    }
    
    public double getSideC() {
        return sideC;
    }

    @Override
    public double calArea() {
        double sumDTwo = (this.sideA + this.sideB + this.sideC)/2;
        return Math.sqrt(sumDTwo*(sumDTwo-this.sideA)*(sumDTwo-this.sideB)*(sumDTwo-this.sideC));
    }

    @Override
    public double calPerimeter() {
        return this.sideA + this.sideB + this.sideC;
    }

    @Override
    public String toString() {
        return this.getName() + " sideA: " + this.sideA + " sideB: " + this.sideB + " sideC: " + this.sideC;
    }
}
