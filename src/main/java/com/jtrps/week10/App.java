package com.jtrps.week10;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Rectangle rec1 = new Rectangle(5, 3);
        System.out.println(rec1.toString());
        System.out.println(rec1.calArea());
        System.out.println(rec1.calPerimeter());

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.println(rec2.calArea());
        System.out.println(rec2.calPerimeter());

        Circle circle1 = new Circle(2);
        System.out.println(circle1);
        System.out.printf("%.3f \n",circle1.calArea());
        System.out.printf("%.3f \n",circle1.calPerimeter());

        Circle circle2 = new Circle(3);
        System.out.println(circle2);
        System.out.printf("%.3f \n",circle2.calArea());
        System.out.printf("%.3f \n",circle2.calPerimeter());

        Triangle triangle1 = new Triangle(2, 2, 2);
        System.out.println(triangle1);
        System.out.printf("%.3f \n",triangle1.calArea());
        System.out.println(triangle1.calPerimeter());
    }
}
